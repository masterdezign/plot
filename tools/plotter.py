"""
File: plotter.py
Version: 1.0
Author: Bogdan Penkovskyi
Description: Basic plotter

Among its features:
    * loading data from separate files
      with plotting consequently on one frame
    * setting legend for each of them
    * labeling axes and giving plot a title
"""

import pylab as pl
from matplotlib.backends.backend_agg import FigureCanvasAgg
from mpl_toolkits.mplot3d import Axes3D

class PlotterBase(object):
    """ Abstract class
    """
    def __init__(self, **kwarg):
        self.__dict__.update(kwarg)
        self.label_font_size = 28
        # Plot init
        self.init_plot()

    def init_plot(self):
        """ General canvas preparation
        """
        for labname in [ 'title', 'xlabel', 'ylabel', 'zlabel' ]:
            if labname in self.__dict__:
                getattr(self.ax, "set_%s" % labname)(
                    self.__dict__[labname],
                    color='#000000',
                    size=self.label_font_size)

        self.ax.grid(True)

    def before_render(self):
        """ Called just before render
        """
        handles, labels = self.ax.get_legend_handles_labels()
        self.ax.legend(handles, labels)

    def show(self):
        """ Shows figure
        """
        self.before_render()
        pl.show()

    def save(self, filename, format='pdf',
             transparent=True):
        """ Saves figure
        """
        self.before_render()
        canvas = FigureCanvasAgg(self.fig)
        canvas.print_figure("%s.%s" % (filename, format),
                            format=format,
                            transparent=transparent)
        # Clear fig to free memory
        self.fig.hold(False)
        del self.fig
        pl.close()


class Plotter(PlotterBase):
    """ Basic plot wrapper
    """
    def __init__(self, **arg):
        super(Plotter, self).__init__(**arg)

    def init_plot(self):
        """ General canvas preparation
        """
        self.fig = pl.figure()
        self.ax = self.fig.add_subplot(111)
        super(Plotter, self).init_plot()

    def plot(self, data, marker='-', from_points=True, **kwarg):
        """ from_points should be True if
        input data like [(1, 2), (x, y)...]
        and False if, [[1, x, ...], [2, y, ...]]
        """
        x, y = zip(*data) if from_points else data
        if 'lw' not in kwarg:
            kwarg['lw'] = 4  # Default line width
        self.ax.plot(x, y, marker, **kwarg)


class Plotter3D(PlotterBase):
    """ Plots in 3D space
    """
    def __init__(self, **arg):
        super(Plotter3D, self).__init__(**arg)

    def init_plot(self):
        """ General canvas preparation
        """
        self.fig = pl.figure()
        self.ax = Axes3D(self.fig)
        ## self.ax.set_xbound(-2.5, 2.5)
        ## self.ax.set_ybound(-2.5, 2.5)
        # self.ax.set_autoscale_on(False)
        # self.ax.set_xlim3d(-2.5, 2.5)
        # self.ax.set_ylim3d(-2.5, 2.5)
        # self.ax.set_zlim3d(-2.5, 2.5)

        super(Plotter3D, self).init_plot()

    def plot(self, data, from_points=True, **kwarg):
        """ from_points should be True if
        input data like [(1, 2, 0.5), (x, y, z)...]
        and False if, [[1, x, ...], [2, y, ...], [0.5, z, ...]]
        """
        x, y, z = zip(*data) if from_points else data
        self.ax.scatter(x, y, z, **kwarg)


def main():
    """ Main routine
    """
    p = Plotter3D(xlabel="$X$",
                  ylabel="$Y$",
                  zlabel="$Z$",
                  title="N=10")
    p.plot([(1, 1, 0.1), (2, 2.5, 0.5)],
           label="Some plot A",
           color='#b00000')
    p.plot([(1, 1.5, 0.2), (2, 2.25, 0.5)], label="Some plot B")
    # p.save('test_3d')
    p.show()

    p = Plotter(xlabel="$\epsilon$",
                ylabel="$\delta$",
                title="N=10")
    p.plot([(1, 1), (2, 2.5)], label="Some plot A")
    p.plot([(1, 1.5), (2, 2.25)], label="Some plot B")
    # p.save('test_2d')
    p.show()


if __name__ == '__main__':
    main()
