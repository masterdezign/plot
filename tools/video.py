"""
File: video.py
Author: Bogdan Penkovskyi
Description: Video from frames helpers
"""

import os
from math import log10


def make(image_dir, clean=False):
    """ Makes video
    """
    _render_dir(image_dir)
    if clean:
        _cleanup(image_dir)


class Prefixed(object):
    """ Prefixes files to render in specified
    directory.

    >>> p = Prefixed('png')
    >>> p.next()
    '00000001.png'

    >>> p.next()
    '00000002.png'

    >>> p = Prefixed('jpg', dirname='t/td')
    >>> p.next()
    't/td/00000001.jpg'
    """

    def __init__(self, filetype=None, dirname=None):
        self.filetype = filetype
        self.dirname = dirname
        self.free_id = 0

    def next(self):
        """ Returns next file name
        """
        self.free_id += 1
        fname = _prefix(self.free_id)
        if self.filetype:
            fname = "%s.%s" % (fname, self.filetype)
        return os.path.join(self.dirname, fname) if self.dirname else fname


def _prefix(number, max_prefix_len=8):
    """ Creates prefix used to name files for video rendering

    >>> _prefix(22, max_prefix_len=3)
    '022'
    """
    return '0' * ((max_prefix_len - 1) - int(log10(number))) + str(number)


def _render_dir(image_dir, filetype="png"):
    """
    Creates video from <filetype> files

    mencoder -ovc xvid -xvidencopts pass=2:bitrate=15999:max_bframes=0 \
     -oac copy -mf fps=25:type=png 'mf://*.png' -vf harddup -ofps 25 \
     -noskip -of avi -o movie_1.avi
    """
    cmd = "mencoder -ovc xvid -xvidencopts pass=2:bitrate=15999:max_bframes=0"
    cmd += " -oac copy -mf fps=40:type=%s 'mf://%s/*.%s' -vf harddup -ofps 40"
    cmd += " -noskip -of avi -o %s/movie.avi"
    os.system(cmd % (filetype, image_dir, filetype, image_dir))


def _cleanup(image_dir, filetype="png"):
    """
    Removes all <filetype> files in dir
    """
    os.system("rm %s/*.%s" % (image_dir, filetype))
