"""
File: extract.py
Author: Bogdan Penkovskyi
Description: Extracts information about excitatory and
inhibitory x coordinates from the given file.
"""

import os

import numpy as np

from tools.plotter import Plotter
from tools.video import Prefixed
from tools.video import make as make_video


def reader(fun):
    """ Reads data from file
    and passes it to decorated function
    """
    def _decorate(filename, *arg, **kwarg):
        """
        max_lines   -- number of lines to read
        total_lines -- number of lines to process finally
        """
        rfile = open(filename)
        max_lines = kwarg['max_lines']
        data = rfile.readlines()[:max_lines]
        if 'total_lines' in kwarg:
            data = data[-kwarg['total_lines']:]
        rfile.close()

        return fun(data, *arg, **kwarg)
    return _decorate


@reader
def load_data(data, max_lines=None, dim=3, total_lines=0):
    """ Loads 3-dimensional data in point form.
    """
    assert(len(data[0].split()) % dim == 0)
    expected_points_in_line = len(data[0].split()) // dim

    result = []

    for line in data:
        line_points = []
        point = []  # To avoid warnings, never used, though

        for i, coord in enumerate(line.split()):
            # Create a new point for each (x, y, z)
            if i % dim == 0:
                # If we already have a point
                if i > 0:
                    line_points.append(point)
                # Create a new point
                point = []
            # Add coordinate for the point
            point.append(float(coord))

        # Not to forget the last point
        line_points.append(point)

        # Check for unexpected number of points
        assert(len(line_points) == expected_points_in_line)

        result.append(line_points)

    return result


def filt(data, coordinate):
    """ Filters x variables
    """
    resulting_x = []
    coord_shift = {
        'x': 0,
        'y': 1,
        'z': 2
    }[coordinate]

    for line in data:
        resulting_x.append([point[coord_shift]
                            for point in line])
    return resulting_x


def plot_x1x2(data):
    """ Plots x_1 versus x_2
    """
    xy = [(d[0], d[1]) for d in data]

    p = Plotter('x_1', 'x_2', 'Rossler oscillators')
    p.plot(xy)
    p.show()


def plot_xy(data, total=2000):
    """ Plots x_1 versus y_1
    Only last <total> iterations are plotted.
    """
    p = Plotter('x_1', 'y_1', 'Rossler oscillators')
    # line[0] is first oscillator
    xy = [(line[0][0], line[0][1]) for line in data[-total:]]
    p.plot(xy, lw=0.1)
    p.show()


def plot_x(data_with_labels):
    """ Just plots values of one coordinate
    """
    p = Plotter('oscillator', 'phase')
    for label in data_with_labels:
        y_data = data_with_labels[label]
        x_data = np.arange(1, 1 + len(y_data), 1)
        p.plot([x_data, y_data], from_points=False, label=label,
                  marker='o')
    p.show()


def make_animation(data, dirname, num_of_final=100, clean=False):
    """ Animates data with background of final trajectory (num_of_final iterations)

    dirname -- directory with movie
    """
    final_data = data[-num_of_final:]
    N = len(data[0]) // 2

    if not os.path.exists(dirname):
        os.mkdir(dirname)

    pref = Prefixed(dirname=dirname)

    def get_bg():
        """ Returns common background.
        Should be optimized not to generate it each time,
        i.e saved in separate file.
        """
        bg_trajec = Plotter('x_i', 'y_i', 'Rossler oscillators $\sigma=0.68,$ $\epsilon=0.4$')
        # Plot trajectory of the first oscillator
        xy = [(line[0][0], line[0][1]) for line in final_data]
        bg_trajec.plot(xy, lw=0.5, color="#aaaaaa")
        return bg_trajec

    def plot_group(group_data, plot_obj, label, color):
        """ Plots a group of oscillators
        """
        group = [(point[0], point[1]) for point in group_data]
        plot_obj.plot(group, marker='o', label=label, color=color)
        return plot_obj

    # Render each image
    for i, line in enumerate(data):
        frame_name = pref.next()
        # Print progress
        print frame_name, "(%.1f%%)" % ((i + 1)* 100.0 / len(data))

        new_frame = get_bg()
        new_frame = plot_group(line[:N], new_frame, 'Attractive', '#e00000')
        new_frame = plot_group(line[N:], new_frame, 'Repulsive', '#00a000')
        new_frame.save(frame_name, format="png")

    # Create animation
    make_video(dirname, clean=clean)


if __name__ == '__main__':
    from optparse import OptionParser
    OP = OptionParser(
        usage= "%prog filename [-p | -a <dir for anim> [-c] | -t]",
        version="%prog 1.0")
    OP.add_option("-m", "--max-lines",
                  dest="max_lines",
                  help="Maximal number of lines to read from file",
                  default="-1")
    OP.add_option("-a", "--animation",
                  dest="animation",
                  help="Create video animation in given directory (if no -p flag)",
                  default=False)
    OP.add_option("-c", "--clean",
                  dest="clean",
                  help="Remove temporary files",
                  action="store_true",
                  default=False)
    OP.add_option("-p", "--plot",
                  dest="plot",
                  help="Plots x_1 vs x_2",
                  action="store_true",
                  default=False)
    OP.add_option("-t", "--text-only",
                  dest="text",
                  help="Do not plot image",
                  action="store_true",
                  default=False)

    (OPTIONS, ARGS) = OP.parse_args()

    fname = ARGS[0]
    MAX_LINES = int(OPTIONS.max_lines)

    if OPTIONS.plot:
        plot_xy(load_data(fname, max_lines=MAX_LINES))
        # plot_x1x2(
        #     filt(load_data(fname, max_lines=MAX_LINES),
        #          'x'))
    elif OPTIONS.animation:
        make_animation(load_data(fname, max_lines=MAX_LINES), OPTIONS.animation,
                       clean=OPTIONS.clean)
    else:
        DATA = load_data(fname,
                         max_lines=MAX_LINES,
                         total_lines=1  # Makes loader process only one line
                        )
        prepare = lambda coord: ', '.join(map(lambda k: "%.3f" % k,
                                        filt(DATA, coord)[-1]))

        print "x:", prepare('x')
        print "y:", prepare('y')
        print "z:", prepare('z')

        if not OPTIONS.text:
            plot_x({
                   'x': filt(DATA, 'x')[-1],
                   'y': filt(DATA, 'y')[-1],
                   'z': filt(DATA, 'z')[-1],
            })
